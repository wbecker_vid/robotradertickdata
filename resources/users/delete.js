// TODO: DELETE files with parentID = this.id
if (!(me && me.roles.indexOf('admin') !== -1)) {
  cancel('You must be an admin to delete users', 401);
}
emit('users:del', this);
