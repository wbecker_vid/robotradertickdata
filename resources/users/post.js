if (!(me && me.roles.indexOf('admin') !== -1)) {
  cancel('You must be an admin to create users', 401);
}

this.createdDate = new Date().getTime();
this.createdBy = me.id;
this.modifiedDate = new Date().getTime();
this.modifiedBy = me.id;
emit('users:post', this);
