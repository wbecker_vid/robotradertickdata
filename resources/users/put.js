if (!(me)) {
  cancel('You must be loged in to edit users', 401);
}
protect('createdDate');
protect('mandant');
this.modifiedDate = new Date().getTime();
this.modifiedBy = me.id;
emit('users:put', this);
