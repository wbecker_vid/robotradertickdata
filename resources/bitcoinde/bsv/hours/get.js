if (parts && parts.length === 1) {

  console.log('bitcoinde/bsv/hours', parts);
  const lookBackHours = parts[0];

  const minDate = new Date().getTime() - (1000 * 60 * 60 * lookBackHours);
  const dataquery = {
    t: {$gt: minDate}, $sort: {t: 1}
  };
  dpd['bitcoinde' + 'bsv'].get(dataquery, (result, error) => {
    if (error) {
      setResult('getRawChartData ERROR: ' + error);
    } else {
      setResult(result);
    }
  });
}
