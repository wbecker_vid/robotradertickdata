const deployd = require('deployd');
const winston = require('winston');
const request = require('request');

// Logging and Error Handling
// =============================================================================
winston.configure({
  level: 'info',
  format: winston.format.combine(winston.format.timestamp(), winston.format.simple()),
  transports: [new (winston.transports.Console)(), new (winston.transports.File)({
    format: winston.format.timestamp(), filename: 'errors.log', level: 'error'
  })]
});

process.on('uncaughtException', function (err) {
  winston.error('uncaughtException: ' + err.message, err);
});

const server = deployd({
  port: 2803, env: 'production', db: {
    connectionString: 'mongodb://user:Fax68DogEtnhzMcka2ui@127.0.0.1:27017/vidData'
  }
});

server.listen();

server.on('listening', function () {
  winston.info('deployd => Server is listening');
});

server.on('error', function (err) {
  winston.error('deployd server error: ' + err.message, err);
  process.nextTick(function () { // Give the server a chance to return an error
    process.exit();
  });
});

// run tickdata
setInterval(function () {
  try {
    const url = 'https://data.visionate.com/robotrader/tickdata';
    const qs = {getOrderCharts: true};
    request.get({url: url, qs: qs, json: true}, function (error, response, body) {
      if (error) {
        winston.error('data.visionate.com/robotrader/tickdata :error ' + JSON.stringify(error, null, ' '));
      } else {
        winston.info('data.visionate.com/robotrader/tickdata sucess');
      }
      winston.info('data.visionate.com/robotrader/tickdata statusCode:' + response.statusCode);
    });
  } catch (e) {
    winston.error('error on data.visionate.com/robotrader/tickdata', e);
  }
}, 2 * 60 * 1000); // 2 mins

// run process User Indicators
setInterval(function () {
  try {
    const url = 'https://europe-west2-robotrade-btc.cloudfunctions.net/processUserIndicators';
    request.get({url: url}, function (error, response, body) {
      if (error) {
        winston.error('europe-west2-robotrade-btc.cloudfunctions.net/processUserIndicators :error ' + JSON.stringify(error, null, ' '));
      } else {
        winston.info('europe-west2-robotrade-btc.cloudfunctions.net/processUserIndicators body:' + body);
      }
      winston.info('europe-west2-robotrade-btc.cloudfunctions.net/processUserIndicators statusCode:' + response.statusCode);
    });
  } catch (e) {
    winston.error('error on europe-west2-robotrade-btc.cloudfunctions.net/processUserIndicators', e);
  }
}, 5 * 60 * 1000); // 5 mins
