import * as _ from 'lodash';
import { AverageParameters, OHLCEntry } from '../models/indicator.model';

export enum Signum {
  baerish = 1, neutral = 0, bullish = -1
}

export type Sign = -1 | 0 | 1 ;

export class IndicatorTools {

  /**
   *
   * @returns {number}
   */
  public static arrayAvarage( parameters: AverageParameters ): number {
    const { data, size } = parameters;
    if ( !IndicatorTools.isArrayOrArrayClass( data ) ) {
      throw new TypeError( 'data is not Array' );
    }

    const _size = !size ? data.length : size;

    let avarage = 0;
    for ( let i = 0; i < _size; i++ ) {
      avarage += data[ i ];
    }
    return avarage / _size;
  }

  /**
   *
   * @param {number} value
   * @returns Sign
   */
  public static sign( value: number ): Sign {
    if ( value > 0 ) {
      return 1;
    }
    if ( value < 0 ) {
      return -1;
    }
    return 0;
  }

  public static roundToMinute( milliSeconds: number ): number {
    const p = 60 * 1000; // milliseconds in a minute
    return Math.round( milliSeconds / p ) * p;
  }

  public static number2OHLC( data: number[], times?: number[] ): OHLCEntry[] {
    if ( !IndicatorTools.isArrayOrArrayClass( data ) ) {
      throw new TypeError( 'data is not Array' );
    }
    if ( times && !IndicatorTools.isArrayOrArrayClass( times ) ) {
      throw new TypeError( 'times is not Array' );
    }
    // data as close
    // open ist der linke wert, also data[i-1]
    const open: number[] = _.take( data, data.length - 1 );
    // ersten close als ersten open
    open.unshift( data[ 0 ] );

    if ( times ) {
      return data.map( ( value: number | undefined, index ) => {
        const o = index === 0 ? value : open[ index ];
        return {
          o: o || undefined,
          c: value || undefined,
          t: times[ index ]
        } as OHLCEntry;
      } );
    } else {
      return data.map( ( value: number | undefined, index ) => {
        const o = index === 0 ? value : open[ index ];
        return {
          o: o || undefined,
          c: value || undefined
        } as OHLCEntry;
      } );
    }

  }

  /**
   *
   * @param {OHLCEntry[]} data
   * @param {number} intervalMilliseconds
   * @param {boolean} calcAvarage
   * @param numBars
   * @returns {OHLCEntry[]}
   */
  public static reduceToInterval( data: OHLCEntry[], intervalMilliseconds: number, calcAvarage: boolean,
    numBars?: number ): OHLCEntry[] {

    if ( !IndicatorTools.isArrayOrArrayClass( data ) ) {
      throw new TypeError( 'data is not Array' );
    }
    if ( typeof intervalMilliseconds !== 'number' || isNaN( intervalMilliseconds ) ) {
      throw new TypeError( 'intervalMilliseconds is not a Number' );
    }

    const startTime: number = IndicatorTools.roundToMinute( data[ data.length - 1 ].t );
    const endTime: number = IndicatorTools.roundToMinute( data[ 0 ].t );
    const millisecondsAvailable = startTime - endTime;
    if ( !numBars ) {
      numBars = Math.floor( millisecondsAvailable / intervalMilliseconds );
    }
    // const numBars = Math.floor( millisecondsAvailable / intervalMilliseconds );
    const returnArray: OHLCEntry[] = [];
    let index = 0;
    let lastIntervalTime = startTime;
    while ( index < numBars ) {
      const intervalTime = startTime - (intervalMilliseconds * index);
      const items: OHLCEntry[] = data.filter(
        item => item.t >= (intervalTime - intervalMilliseconds) && item.t < lastIntervalTime );
      // console.log( 'reduceToInterval => data', data, intervalMilliseconds, numBars, startTime );
      if ( typeof items !== 'undefined' && items !== null && items.length > 0 ) {
        if ( calcAvarage ) {
          returnArray.push( {
            t: intervalTime,
            o: IndicatorTools.arrayAvarage( { data: items.map( item => item.o as number ) } ),
            h: IndicatorTools.arrayAvarage( { data: items.map( item => item.h as number ) } ),
            l: IndicatorTools.arrayAvarage( { data: items.map( item => item.l as number ) } ),
            c: IndicatorTools.arrayAvarage( { data: items.map( item => item.c as number ) } ),
            v: IndicatorTools.arrayAvarage( { data: items.map( item => item.v as number ) } )
          } as OHLCEntry );
        } else {
          returnArray.push( {
            t: intervalTime,
            o: items[ 0 ].o,
            h: items[ 0 ].h,
            l: items[ 0 ].l,
            c: items[ 0 ].c,
            v: items[ 0 ].v
          } );
        }
      }
      /* else {
       returnArray.push( {
       time: intervalTime,
       value: NaN
       } );
       }*/
      index++;
      lastIntervalTime = intervalTime;
    }
    /*const debugData: any[] = returnArray.map( ( item ) => {
     item[ 'date' ] = new Date( item.t ).toLocaleTimeString( 'de', {
     year: 'numeric',
     month: 'numeric',
     day: 'numeric'
     } );
     return item;
     } );
     console.log( 'reduceToInterval', debugData, data, intervalMilliseconds, numBars );*/
    _.reverse( returnArray );
    return returnArray;
  }

  /**
   *
   * @param {number} value
   * @param {number} decimalPlaces
   * @returns {number}
   */
  public static round_number( value: number, decimalPlaces: number ) {
    return Math.floor( value * Math.pow( 10, decimalPlaces ) ) / Math.pow( 10, decimalPlaces );
  }

  public static isArrayOrArrayClass( clazz: any ): boolean {
    return Array.isArray( clazz );
  }

  public static isValidNumber( val ): boolean {
    return typeof val !== 'undefined' && !isNaN( val );
  }

}
