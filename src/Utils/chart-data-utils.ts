import { max, min, sum } from 'lodash';
import * as sequential from 'promise-sequential';
import { BitcoinDeClient } from '../bitcoinDeAPI/bitcoin-de-client';
import { CompactOrderResult, CryptoCurrency, OrderBook, TradingPair, TradingPairByCryptoCurrency } from '../bitcoinDeAPI/order-book';

export type ChartEntrys = {
  bids: OHLCVEntry, asks: OHLCVEntry
}
export type Entry = {
  id: number, time: number, price: number, volume: number
}
export type PriceVolumeEntry = {
  t: number, p: number, v: number
}
export type OHLCVEntry = {
  t: number, o: number, h: number, l: number, c: number, v: number
}
export type BitcoinDeChartEntry = {
  t: number, c: number, asks: {
    c: number, h: number, l: number, o: number, v: number
  }, bids: {
    c: number, h: number, l: number, o: number, v: number
  }
}

export interface ChartEntry {
  high: number,
  low: number,
  priceAvarage: number,
  volume: number
}

export class ChartDataUtils {

  /**
   *
   * @param trading_pair
   * @param orderBook
   * @param marketClose
   * @param dpd
   */
  public static setTickData( trading_pair: TradingPair, orderBook: OrderBook, marketClose: number, dpd ): Promise<ChartEntrys> {
    return new Promise( ( resolve, reject ) => {

      // showOrderbookCompact - Kauf- und Verkaufsangebote (bids und asks) in kompakter Form.
      orderBook.orderscompact( trading_pair, async ( error, result ) => {
        if ( error ) {
          reject( 'setTickData => orderscompact => error: ' + error.toString() );
        } else {
          console.log( result );
          // result:
          // "orders":{
          //    "bids": [ { "price":200, "amount":0.2,},...],
          //    "asks": [{ "price":250, "amount":0.2,},...]
          // },
          const time: number = Date.now();
          const asks: CompactOrderResult[] = ChartDataUtils.filterTickData( result.orders.asks, marketClose, 5 ); // asks
          // dürfen//
          // mehr
          const bids: CompactOrderResult[] = ChartDataUtils.filterTickData( result.orders.bids, marketClose, 5 ); // bids sind
          // der kurs

          const returnVal: ChartEntrys = {
            asks: ChartDataUtils.getOHLC( asks, time ), // Kaufanfragen (asks)
            bids: ChartDataUtils.getOHLC( bids, time ) // Verkaufsangebote (bids)
          };
          resolve( returnVal );
        }
      } );
    } );
  }

  /**
   * filter Tickdata bigger or less then percentThreshold of marketClose
   * @param data
   * @param marketClose
   * @param percentThreshold
   */
  private static filterTickData( data: CompactOrderResult[], marketClose: number, percentThreshold: number ): CompactOrderResult[] {
    return data.filter( ( item, index, newArray ) => {
      const diff = Math.abs( marketClose - item.price );
      const percentDiff = (diff / marketClose) * 100;
      return percentDiff < percentThreshold;
    } );
  }

  /**
   * cleanUpTickData older then keepDays
   * @param keepDays
   * @param dpd
   */
  public static cleanUpTickData( keepDays: number, dpd ): Promise<string[]> {
    return sequential( OrderBook.cryptoCurrencys.map( ( cryptoCurrency ) => {
      return function ( previousResponse, responses, count ) {
        return ChartDataUtils.cleanupTickdataofChart( cryptoCurrency, keepDays, dpd );
      };
    } ) );
  }

  public static cleanupTickdataofChart( cryptoCurrency: CryptoCurrency, keepDays: number, dpd ): Promise<string[]> {
    return new Promise( ( resolve, reject ) => {
      const tstamp = Date.now() - ((((keepDays * 24) * 60) * 60) * 1000);
      const query = { 't': { $lt: tstamp } };

      console.log( 'cleanupTickdataofChart => query', query );

      dpd[ 'bitcoinde' + cryptoCurrency ].get( query, ( result, err ) => {
        if ( err ) {
          return reject( err );
        } else {
          if ( result && result.length && result.length > 0 ) {
            /* console.log( 'result.length', result.length );
             const firstDate = new Date( result[ 0 ].t );
             const lastDate = new Date( result[ result.length - 1 ].t );
             console.log( 'result first', firstDate.toLocaleDateString(), firstDate.toLocaleTimeString() );
             console.log( 'result last', lastDate.toLocaleDateString(), lastDate.toLocaleTimeString() );*/
            sequential( result.map( ( item ) => {
              return function ( previousResponse, responses, count ) {
                return new Promise( ( resolve, reject ) => {
                  dpd[ 'bitcoinde' + cryptoCurrency ].del( { 'id': item.id }, ( res, err? ) => {
                    if ( err ) {
                      console.error( 'error on del', err );
                      resolve( item.id );
                    } else {
                      resolve( item.id );
                    }
                  } );
                } );
              };
            } ) )
              .then( ( res ) => {
                resolve( res as any );
              } )
              .catch( e => reject( e ) );

          } else {
            return reject( 'no result' );
          }
        }
      } );
    } );
  }

  /**
   * Gets rates from Bitcoin.de and then gets the orderbook and sets the Tickdata
   */
  public static getOrderCharts( bitcoinDeClient: BitcoinDeClient, orderBook: OrderBook, dpd ): Promise<string> {
    return sequential( OrderBook.cryptoCurrencys.map( ( cryptoCurrency ) => {
      return function ( previousResponse, responses, count ) {
        return ChartDataUtils.getOrderChartsofChart( cryptoCurrency, bitcoinDeClient, orderBook, dpd );
      };
    } ) );
  }

  public static getOrderChartsofChart( cryptoCurrency: CryptoCurrency, bitcoinDeClient: BitcoinDeClient, orderBook: OrderBook, dpd ): Promise<string> {
    return new Promise( ( resolve, reject ) => {
      /** rates abfragen
       "rates":{
         "rate_weighted":"257.3999269", // der gewichtete Durchschnittskurs der letzten 3 Stunden
         "rate_weighted_3h":"258.93994247",
         "rate_weighted_12h":"255.30363219"
       }, */

      bitcoinDeClient
        .get( 'rates', { trading_pair: TradingPairByCryptoCurrency[ cryptoCurrency ] }, ( ratesError, ratesResult ) => {
          if ( ratesError ) {
            console.error( {
                             msg: 'getOrderCharts => error: btcDe.rates failed',
                             error: ratesError.toString()
                           } );
            reject( ratesError );
            return;
          } else {
            const time = Date.now();
            const close = +ratesResult.rates.rate_weighted;
            ChartDataUtils.setTickData( TradingPairByCryptoCurrency[ cryptoCurrency ], orderBook, close, dpd )
                          .then( ( tickDataResults: ChartEntrys ) => {
                            const asks: OHLCVEntry = tickDataResults.asks;
                            const bids: OHLCVEntry = tickDataResults.bids;
                            dpd[ 'bitcoinde' + cryptoCurrency ].post( {
                                                                        t: time,
                                                                        c: close,
                                                                        asks: {
                                                                          c: asks.c,
                                                                          h: asks.h,
                                                                          l: asks.l,
                                                                          o: asks.o,
                                                                          v: asks.v
                                                                        },
                                                                        bids: {
                                                                          c: bids.c,
                                                                          h: bids.h,
                                                                          l: bids.l,
                                                                          o: bids.o,
                                                                          v: bids.v
                                                                        }
                                                                      } );
                            resolve( 'getOrderCharts => done' );
                          } )
                          .catch( setTickDataError => {
                            console.error( 'getOrderCharts setTickData: ' + setTickDataError.toString() );
                            reject( setTickDataError );
                          } );
          }
        } );
    } );

  }

  public static getOHLC( tickData: CompactOrderResult[], time: number ): OHLCVEntry {
    if ( tickData ) {
      const prices: number[] = tickData.map( tick => tick.price );
      const volumes: number[] = tickData.map( tick => tick.amount );
      return {
        t: time,
        o: prices[ 0 ],
        h: max( prices ),
        l: min( prices ),
        c: sum( prices ) / prices.length,
        v: sum( volumes )
      };
    } else {
      return {
        t: time,
        o: NaN,
        h: NaN,
        l: NaN,
        c: NaN,
        v: NaN
      };
    }
  }

  public static getChartEntry( rawOrders: CompactOrderResult[] ): ChartEntry {
    const prices: number[] = rawOrders.map( tick => tick.price );
    const volumes: number[] = rawOrders.map( tick => tick.amount );
    return {
      high: max( prices ),
      low: min( prices ),
      priceAvarage: sum( prices ) / prices.length,
      volume: sum( volumes )
    };
  }

}
