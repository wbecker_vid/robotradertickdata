import { BitcoinDeClient } from './bitcoinDeAPI/bitcoin-de-client';
import { OrderBook } from './bitcoinDeAPI/order-book';
import { TickdataGenerator } from './tickdata-generator';

const dpd = require( 'dpd-js-sdk' )( 'https://data.visionate.com' );
dpd.bitcoindebtc = dpd( '/bitcoinde/btc' );

const config = {
  apikey: '3548de183405c6cdd7b2fc3e9af5bd00',
  apisecret: '47a3c51dbaf2df5dc090d563ad434c6b1a646fb7'
};

/*const test = new RoboTraderTickdata( null );
 test.dpd = dpd;*/

const btcDeClient = new BitcoinDeClient( config.apikey, config.apisecret );
const orderBook = new OrderBook( btcDeClient );

const tdg: TickdataGenerator = new TickdataGenerator( dpd, config );

/*
 orderBook.orderscompact( async ( error, result ) => {
 if ( error ) {
 console.error( 'setTickData => orderscompact => error: ' + error.toString() );
 } else {
 console.log( 'Verkaufsangebote(ASKS)', orderBy( result.orders.asks, [ 'price' ], [ 'asc' ] ).slice(0, 5) );
 console.log( 'Kaufanfragen(BIDS)', orderBy( result.orders.bids, [ 'price' ], [ 'desc' ] ).slice(0, 5) );
 }
 });
 */

tdg.getTickdataFromOrderbook( 'btceur', Date.now() )
   .then( ( { asks, bids } ) => {
     console.log( 'Verkaufsangebote(ASKS), Kaufanfragen(BIDS)', asks, bids );
   } )
   .catch( e => console.error( JSON.stringify( e ) ) );

/*
 test.handle( {
 done: ( error, res ) => {
 if ( error ) {
 console.log( 'done:error', error );
 } else {
 console.log( 'done:res', JSON.stringify( res, null, 1 ) );
 }
 },
 query: {
 getOrderCharts: 'true'
 },
 method: 'GET',
 debug: true
 }, () => {

 } );
 */

