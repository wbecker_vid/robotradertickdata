import { OrderModel } from '../models/order-model';
import { TradeUserModel } from '../models/trade-user-model';
import { ChartDataUtils, ChartEntry } from '../Utils/chart-data-utils';
import { BitcoinDeClient } from './bitcoin-de-client';

export enum OrderType {
  buy = 'buy', sell = 'sell'
}

export enum TradingPairs {
  btceur = 'btceur', bcheur = 'bcheur', etheur = 'etheur', btgeur = 'btgeur', bsveur = 'bsveur'
}

export type TradingPair = 'btceur' | 'bcheur' | 'etheur' | 'btgeur' | 'bsveur';

export enum TradingPairByCryptoCurrency {
  btc = 'btceur', // Bitcoin
  bch = 'bcheur', // Bitcoin Cash
  eth = 'etheur', // Ethereum
  btg = 'btgeur', // Bitcoin Gold
  bsv = 'bsveur', // Bitcoin SV
}

export enum CryptoCurrencys {
  btc = 'btc', // Bitcoin
  bch = 'bch', // Bitcoin Cash
  eth = 'eth', // Ethereum
  btg = 'btg', // Bitcoin Gold
  bsv = 'bsv', // Bitcoin SV
}

export type CryptoCurrency = 'btc' | 'bch' | 'eth' | 'btg' | 'bsv';

export enum OrderSeatOfBank {
  AT = 'AT ', CH = 'CH', DE = 'DE'
}

export interface OrderParams {
  /** Angebots-Typ. “buy” liefert Verkaufsangebote , “sell” Kaufangebote */
  type: OrderType;
  /** Handelspaar (s. Tabelle Handelspaare) */
  trading_pair: TradingPair
  /** Optional Menge der Coins */
  amount?: number;
  /** Optional Preis pro Coin in Euro. Entspricht bei "buy" dem maximalen Kaufpreis und bei "sell" dem minimalen Verkaufspreis.*/
  price?: number;
  /** Optional Nur Angebote anzeigen, deren Anforderungen ich erfülle (bspw. Legitimationsstatus, Trust-Level, Sitz der Bank, Zahlungsart). */
  order_requirements_fullfilled?: boolean;
  /** Optional Nur Angebote von vollständig identifizierten Usern anzeigen. */
  only_kyc_full?: boolean; //
  /** Optional Nur Angebote anzeigen, die über Express-Handel gehandelt werden können. */
  only_express_orders?: boolean;
  /** Optional Nur Angebote von Handelspartner anzeigen, die ein Bankkonto bei derselben Bankgruppe (BIC-Nummernkreis) wie ich haben. */
  only_same_bankgroup?: boolean;
  /** Optional Nur Angebote von Handelspartnern anzeigen, die ein Bankkonto bei derselben Bank wie ich haben. */
  only_same_bic?: boolean;
  /** Optional Bankenländerliste,  Alle möglichen Länder aus der Tabelle Bankenländerliste,  Nur Angebote mit bestimmtem Sitz der Bank anzeigen. (ISO 3166-2). s. Tabelle Bankenländerliste */
  seat_of_bank?: OrderSeatOfBank[] | OrderSeatOfBank;
}

export interface OrderResult {

  /** ID des Angebots */
  order_id: string;

  trading_pair: TradingPair;

  type: OrderType;

  max_amount: number;

  min_amount: number;

  price: number;

  max_volume: number;

  min_volume: number;

  order_requirements_fullfilled: boolean;

  trading_partner_information: TradeUserModel;

  order_requirements: any[]
}

export type CompactOrderResult = {
  price: number; amount: number
}

export class OrderBook {

  // public static cryptoCurrencys: CryptoCurrency[] = [ 'btc', 'bch', 'eth', 'btg', 'bsv' ];
  // public static cryptoCurrencys: CryptoCurrency[] = [ 'btc', 'bch', 'eth' ];
  public static cryptoCurrencys: CryptoCurrency[] = [ 'btc' ];

  // public static tradingPair: string = 'btceur';

  // public static currency: string = 'btc';

  public orderParamsDefault: OrderParams = {
    type: undefined,
    trading_pair: undefined,
    only_express_orders: true,
    only_kyc_full: false,
    only_same_bankgroup: false,
    only_same_bic: false,
    order_requirements_fullfilled: true
  };

  private btcDeClient: BitcoinDeClient;

  /**
   *
   * @param btcDeClient
   */
  constructor( btcDeClient: BitcoinDeClient ) {
    this.btcDeClient = btcDeClient;
  }

  /**
   *
   * @param tradingPair
   * @param {Function} callback
   */
  public rates( tradingPair: TradingPair, callback: Function ) {
    this.btcDeClient.get( 'rates', { trading_pair: tradingPair }, callback );
  }

  /**
   *
   * @param {OrderParams} parameters
   */
  public orders( parameters: OrderParams ): Promise<OrderModel[]> {
    return new Promise( ( resolve, reject ) => {
      if ( !parameters.trading_pair ) {
        reject( 'orders => no TradingPair!!' );
      } else {
        this.btcDeClient.get( 'orders', parameters, ( error, result ) => {
          if ( error ) {
            reject( 'OrderBook => orders => error: ' + JSON.stringify( error ) );
          } else {
            resolve( result.orders );
          }
        } );
      }
    } );
  }

  /**
   *
   * @param tradingPair
   * @param {Function} callback
   */
  public orderscompact( tradingPair: TradingPair, callback: Function ) {
    this.btcDeClient.get( 'orders/compact', { trading_pair: tradingPair }, callback );
  }

  /**
   *
   * @param tradingPair
   * @returns {Promise<{ asks: ChartEntry, bids: ChartEntry }>}
   */
  public getOrdersCompact( tradingPair: TradingPair ): Promise<{ asks: ChartEntry, bids: ChartEntry }> {
    return new Promise( ( resolve, reject ) => {
      this.orderscompact( tradingPair, ( error, result ) => {
        if ( error ) {
          console.error( 'btcDe.orderscompact error: ' + error.toString() );
          reject( error );
        } else {
          const returnVal = {
            asks: ChartDataUtils.getChartEntry( result.orders.asks ), // Verkaufsangebote
            bids: ChartDataUtils.getChartEntry( result.orders.bids ) // Kaufanfragen
          };
          resolve( returnVal );
        }
      } );
    } );
  }

  /**
   *
   * @param tradingPair
   * @param {OrderType} requestType
   * @returns {Promise<ChartEntry>}
   */
  public getOrders( tradingPair: TradingPair, requestType: OrderType ): Promise<ChartEntry> {
    return new Promise( ( resolve, reject ) => {
      const orderChartParams = {
        type: requestType,
        trading_pair: tradingPair,
        only_express_orders: true
      };
    } );
  }
}
