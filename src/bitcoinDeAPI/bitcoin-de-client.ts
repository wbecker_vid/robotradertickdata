import * as crypto from 'crypto';
import * as events from 'events';
import * as querystring from 'querystring';
import * as request from 'request';

/**
 * BitcoinDeClient connects to the bitcoin.de API
 * @param {String} key    API Key
 * @param {String} secret API Secret
 */
export class BitcoinDeClient extends events.EventEmitter {
  constructor( key: string, secret: string ) {
    super();
    this.config.key = key;
    this.config.secret = secret;
  }

  public config = {
    url: 'https://api.bitcoin.de',
    version: 'v2',
    agent: 'Bitcoin.de NodeJS API Client',
    timeoutMS: 20000,
    key: '',
    secret: ''
  };

  /**
   * Perform GET API request
   * @param  {String}   method   API method
   * @param  {Object}   params   Arguments to pass to the api call
   * @param  {Function} callback A callback function to be executed when the request is complete
   * @return {Object}            The request object
   */
  public get( method, params, callback ) {
    const url = this.config.url + '/' + this.config.version + '/' + method;
    return this.rawRequest( 'get', url, params, callback );
  };

  /**
   * Perform POST API request
   * @param  {String}   method   API method
   * @param  {Object}   params   Arguments to pass to the api call
   * @param  {Function} callback A callback function to be executed when the request is complete
   * @return {Object}            The request object
   */
  public post( method, params, callback ) {
    const url = this.config.url + '/' + this.config.version + '/' + method;
    return this.rawRequest( 'post', url, params, callback );
  };

  /**
   * Perform DELETE API request
   * @param  {String}   method   API method
   * @param  {Object}   params   Arguments to pass to the api call
   * @param  {Function} callback A callback function to be executed when the request is complete
   * @return {Object}            The request object
   */
  public delete( method, params, callback ) {
    const url = this.config.url + '/' + this.config.version + '/' + method;
    return this.rawRequest( 'del', url, params, callback );
  };

  /**
   * Send the actual HTTP request
   * @param  {String}   method   HTTP method
   * @param  {String}   url      URL to request
   * @param  {Object}   params   POST body
   * @param  {Function} callback A callback function to call when the request is complete
   * @return {Object}            The request object
   */
  private rawRequest( method, url, params, callback ) {
    const nonce = this.noncer();
    let md5Query = 'd41d8cd98f00b204e9800998ecf8427e'; // empty string hash
    const options = {
      url: url,
      timeout: this.config.timeoutMS,
      form: {},
      headers: {}
    };
    if ( params ) {
      switch ( method ) {
        case 'post':
          // console.log( 'rawRequest params:' + JSON.stringify( params ) );
          const queryParams = {};
          Object.keys( params ).sort().forEach( function ( idx ) {
            queryParams[ idx ] = params[ idx ];
          } );
          md5Query = crypto.createHash( 'md5' ).update( querystring.stringify( queryParams ) ).digest( 'hex' );
          // console.log( 'rawRequest queryParams:' + JSON.stringify( queryParams ) );
          options.form = queryParams;
          break;
        case 'get':
        case 'del':
          options.url += '?' + querystring.stringify( params );
          break;
        default:
          const err = new Error( method + ' not defined' );
          this.emit( 'error', err );
          return (typeof callback === 'function' ? callback.call( this, err, null ) : null);
      }
    }
    const signature = crypto.createHmac( 'sha256', this.config.secret )
                            .update(
                              (method === 'del' ? 'DELETE' : method.toUpperCase()) + '#' + options.url + '#' + this.config.key
                              + '#' + nonce + '#' + md5Query ).digest( 'hex' );
    options.headers = {
      'User-Agent': this.config.agent,
      'X-API-KEY': this.config.key,
      'X-API-NONCE': nonce,
      'X-API-SIGNATURE': signature
    };
    return request[ method ]( options, ( error, response, body ) => {
      if ( typeof callback === 'function' ) {
        let data;
        let err;
        if ( error ) {
          err = new Error( 'Error in server response: ' + JSON.stringify( error ) );
          this.emit( 'error', err );
          return callback.call( this, err, null );
        }
        try {
          data = JSON.parse( body );
        } catch ( e ) {
          err = new Error( 'Could not understand response from server: ' + body );
          this.emit( 'error', err );
          return callback.call( this, err, null );
        }
        if ( data.errors && data.errors.length ) {
          err = new Error( 'Bitcoin.de API returned error: ' + data.errors[ 0 ].message );
          this.emit( 'error', err );
          return callback.call( this, err, data.errors );
        } else {
          return callback.call( this, null, data );
        }
      }
    } );
  };

  /**
   * Nonce generator
   */
  private noncer_counter: number = 0;

  private noncer_last: number = 0;

  private noncer() {
    const now = Date.now();
    this.noncer_counter = (now === this.noncer_last ? this.noncer_counter + 1 : 0);
    this.noncer_last = now;
    // add padding to nonce
    const padding = this.noncer_counter < 10 ? '000' : this.noncer_counter < 100 ? '00' : this.noncer_counter < 1000 ? '0' : '';
    return now + padding + this.noncer_counter;
  }
}
