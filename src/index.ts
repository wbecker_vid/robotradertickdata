import { CLog, CLogger } from '@vid/core';
import * as internalClient from 'deployd/lib/internal-client.js';
import * as Resource from 'deployd/lib/resource.js';
import { BitcoinDeClient } from './bitcoinDeAPI/bitcoin-de-client';
import { CryptoCurrency, OrderBook, TradingPairByCryptoCurrency } from './bitcoinDeAPI/order-book';

import { TickdataGenerator } from './tickdata-generator';
import { BitcoinDeChartEntry, ChartDataUtils } from './Utils/chart-data-utils';

let env;
if ( (process as any).server && (process as any).server.options && (process as any).server.options.env ) {
  env = (process as any).server.options.env;
} else {
  env = null;
}

class RoboTraderTickdata extends Resource {

  public logger: CLogger = CLog.createClogger( 'RoboTraderTickdata' );

  public static label = 'RoboTrader Tickdata';

  public static events = [];

  public events;

  public static clientGeneration = true;

  public static basicDashboard = {
    settings: [
      {
        name: 'apikey',
        type: 'text',
        description: 'The bitcoin.de apikey"'
      }, {
        name: 'apisecret',
        type: 'text',
        description: 'The bitcoin.de apisecret"'
      }
    ]
  };

  public clientGeneration = true;

  public config;

  public name;

  public session;

  public dpd: any;

  constructor( options ) {
    super( options );

    Resource.apply( this, arguments );

    this.config = {
      apikey: this.config.apikey || '3548de183405c6cdd7b2fc3e9af5bd00',
      apisecret: this.config.apisecret || '47a3c51dbaf2df5dc090d563ad434c6b1a646fb7'
    };
  }

  /**
   * Handle requests
   * @param ctx
   * @param next
   */
  public handle( ctx, next ) {

    // session um einen client zu erzeugen
    if ( !ctx.debug ) {
      if ( ctx.session ) {
        this.dpd = internalClient.build( (process as any).server, ctx.session, null, ctx );
      } else {
        this.dpd = internalClient.build( (process as any).server, null, null, ctx );
      }
    }

    if ( ctx.method === 'GET' && ctx.query ) {

      if ( ctx.query.getOrderCharts ) {

        const message = ctx.query.getOrderCharts;
        this.logger.info( 'handle get: getOrderCharts  %j', message );

        const btcDeClient = new BitcoinDeClient( this.config.apikey, this.config.apisecret );
        const orderBook = new OrderBook( btcDeClient );

        ChartDataUtils.cleanUpTickData( 7, this.dpd )
                      .finally( () => {
                        this.getTickData()
                            .then( ( res ) => {
                              console.log( 'getTickData res', res );
                              ctx.done( null, res );
                            } )
                            .catch( e => {
                              console.error( 'getTickData error ', e );
                              ctx.done( null, JSON.stringify( e ) );
                            } );
                      } );
      }

    } else {
      next();
    }
  }

  private getTickData() {
    const tdg: TickdataGenerator = new TickdataGenerator( this.dpd, this.config );
    const time = Date.now();
    return this.getTickDataforChart( 'btc', tdg, time );
    /*return sequential( OrderBook.cryptoCurrencys.map( ( cryptoCurrency ) => {
     return ( previousResponse, responses, count ) => {
     return this.getTickDataforChart( cryptoCurrency, tdg, time );
     };
     } ) );*/
  }

  private getTickDataforChart( cryptoCurrency: CryptoCurrency, tdg: TickdataGenerator, time ) {
    return new Promise( ( resolve, reject ) => {
      tdg.getTickdataFromOrderbook( TradingPairByCryptoCurrency[ cryptoCurrency ], time )
         .then( ( { asks, bids } ) => {
           if ( !asks || !bids ) {
             console.error( 'getTickdataFromOrderbook: asks, bids', asks, bids );
             reject( 'no proper Data!' );
           } else {
             const minDate = Date.now() - (1000 * 60 * 60 * 0.2);
             const query = {
               t: { $gt: minDate },
               $sort: { t: 1 }
             };

             this.dpd[ 'bitcoinde' + cryptoCurrency ].get( query, ( result, error ) => {
               if ( error ) {
                 console.error( 'getOrderCharts get latestEntry: ' + error.toString() );
                 reject( error );
               } else {

                 const newEntry: BitcoinDeChartEntry = {
                   t: time,
                   c: bids.c,
                   asks: asks,
                   bids: bids
                 };

                 const latestEntry: BitcoinDeChartEntry = result[ result.length - 1 ];
                 if ( latestEntry ) {
                   // set open values from latestEntry Close Values
                   newEntry.asks.o = latestEntry.asks.c;
                   newEntry.bids.o = latestEntry.bids.c;
                 } else {
                   newEntry.asks.o = newEntry.asks.c;
                   newEntry.bids.o = newEntry.bids.c;
                 }

                 this.dpd[ 'bitcoinde' + cryptoCurrency ]
                   .post( newEntry, ( result, error ) => {
                     setTimeout( () => {
                       resolve( newEntry );
                     }, 5000 );
                   } );
               }
             } );
           }
         } )
         .catch( getTickdataFromOrderbookError => {
           console.error( 'getTickdataFromOrderbook Error: ' + getTickdataFromOrderbookError.toString() );
           resolve( {} );
         } );
    } );
  }

}

module.exports = RoboTraderTickdata;
