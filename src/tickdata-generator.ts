import { CLog, CLogger } from '@vid/core';
import { max, min, orderBy, sum } from 'lodash';
import * as sequential from 'promise-sequential';
import { BitcoinDeClient } from './bitcoinDeAPI/bitcoin-de-client';
import { OrderBook, OrderType, TradingPair } from './bitcoinDeAPI/order-book';
import { OrderModel } from './models/order-model';
import { ChartEntrys, OHLCVEntry } from './Utils/chart-data-utils';
import { IndicatorTools } from './Utils/indicator-tools';

export class TickdataGenerator {

  public logger: CLogger = CLog.createClogger( 'TickdataGenerator' );

  private dpd;

  private config;

  constructor( dpd, config ) {
    this.dpd = dpd;
    this.config = config;
  }

  public getTickdataFromOrderbook( trading_pair: TradingPair, time ): Promise<ChartEntrys> {
    return new Promise( ( resolve, reject ) => {
      const btcDeClient = new BitcoinDeClient( this.config.apikey, this.config.apisecret );
      const orderBook = new OrderBook( btcDeClient );
      const orderTypes = [ OrderType.buy, OrderType.sell ];
      sequential( orderTypes.map( ( orderType ) => {
        return function ( previousResponse, responses, count ) {
          return new Promise( ( resolve1, reject1 ) => {
            orderBook.orders( {
                                type: orderType,
                                trading_pair: trading_pair,
                                only_express_orders: true
                              } )
                     .then( ( ordersRes ) => {
                       setTimeout( () => {
                         resolve1( ordersRes );
                       }, 1000 );
                     } )
                     .catch( e => reject1( e ) );
          } );
        };
      } ) )
        .then( ( res: OrderModel[] ) => {
          const asks = orderBy( res.filter( order => order.type === OrderType.sell ), [ 'price' ], [ 'asc' ] );  // Verkaufsangebote (asks)
          const bids = orderBy( res.filter( order => order.type === OrderType.buy ), [ 'price' ], [ 'desc' ] );  // Kaufanfragen (bids)
          /*this.logger.info( 'asks', asks[ 0 ].price );
           this.logger.info( 'bids', bids[ 0 ].price );*/
          const returnVal: ChartEntrys = {
            asks: TickdataGenerator.getOHLC( asks, time ),
            bids: TickdataGenerator.getOHLC( bids, time )
          };
          resolve( returnVal );
        } )
        .catch( e => reject( 'TickdataGenerator => getTickdataFromOrderbook => error: ' + JSON.stringify( e ) ) );
    } );
  }

  public static getOHLC( tickData: OrderModel[], time: number ): OHLCVEntry {
    if ( tickData ) {
      const prices: number[] = tickData.map( tick => tick.price );
      const volumes: number[] = tickData.map( tick => tick.max_volume );
      return {
        t: time,
        o: NaN,
        h: max( prices ),
        l: min( prices ),
        c: IndicatorTools.arrayAvarage( {
                                          data: prices.slice( 0, 10 ),
                                          size: 10
                                        } ),
        v: sum( volumes )
      };
    } else {
      return {
        t: time,
        o: NaN,
        h: NaN,
        l: NaN,
        c: NaN,
        v: NaN
      };
    }
  }
}
