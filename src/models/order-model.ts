import { JsonProperty } from '@vid/core';
import { OrderParams, OrderType, TradingPair } from '../bitcoinDeAPI/order-book';

import { TradeUserModel } from './trade-user-model';

export class OrderModel implements OrderParams {
  @JsonProperty( 'order_id' ) order_id: string = '';

  @JsonProperty( 'trading_pair' ) trading_pair: TradingPair | undefined = undefined;

  @JsonProperty( 'type' ) type: OrderType;

  @JsonProperty( 'max_amount' ) max_amount: number = NaN;

  @JsonProperty( 'min_amount' ) min_amount: number = NaN;

  @JsonProperty( 'price' ) price: number = NaN;

  @JsonProperty( 'max_volume' ) max_volume: number = NaN;

  @JsonProperty( 'min_volume' ) min_volume: number = NaN;

  @JsonProperty( {
                   name: 'trading_partner_information',
                   clazz: TradeUserModel
                 } ) trading_partner_information: TradeUserModel | undefined = undefined;

  @JsonProperty( 'order_requirements_fullfilled' ) order_requirements_fullfilled: boolean | undefined = undefined;
}
