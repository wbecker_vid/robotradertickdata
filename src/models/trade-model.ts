import { dateStringConverter, JsonProperty } from '@vid/core';

import { TradeUserModel } from './trade-user-model';

export class TradeModel {

  @JsonProperty( 'trade_id' ) trade_id: string = '';

  @JsonProperty( 'isSimulated' ) isSimulated: boolean | undefined = undefined;

  @JsonProperty( 'trading_pair' ) trading_pair: string = '';

  @JsonProperty( 'type' ) type: string = '';

  @JsonProperty( 'amount' ) amount: number = NaN;

  @JsonProperty( 'price' ) price: number = NaN;

  @JsonProperty( 'volume' ) volume: number = NaN;

  @JsonProperty( 'fee_eur' ) fee_eur: number = NaN;

  @JsonProperty( 'fee_btc' ) fee_btc: number = NaN;

  @JsonProperty( 'fee_currency' ) fee_currency: number = NaN;

  @JsonProperty( 'new_trade_id_for_remaining_amount' ) new_trade_id_for_remaining_amount: string = '';

  @JsonProperty( 'state' ) state: number = NaN;

  @JsonProperty( 'my_rating_for_trading_partner' ) my_rating_for_trading_partner: string = '';

  @JsonProperty( {
    name: 'trading_partner_information',
    clazz: TradeUserModel
  } ) trading_partner_information: TradeUserModel | undefined = undefined;

  @JsonProperty( 'payment_method' ) payment_method: number = NaN;

  @JsonProperty( {
    name: 'created_at',
    customConverter: dateStringConverter
  } ) created_at: Date | undefined = undefined;

  @JsonProperty( {
    name: 'successfully_finished_at',
    customConverter: dateStringConverter
  } ) successfully_finished_at: Date | undefined = undefined;
}
