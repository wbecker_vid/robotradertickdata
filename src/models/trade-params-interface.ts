export enum TradeType {
  buy = 'buy', sell = 'sell'
}

export enum TradeState {
  'Cancelled' = -1, 'Pending' = 0, 'Successful' = 1
}

export interface TradeParams {
  /** Trade-Typ */
  type: TradeType;
  /** Handelspaar (s. Tabelle Handelspaare) */
  trading_pair: string;
  /** Optional Aktueller Trade-Status */
  state?: TradeState;
  /** Optional Startzeitpunkt, ab dem Trades zurückgeliefert werden. Format gemäß RFC 3339 (Bsp: 2015-01-20T15:00:00+02:00). */
  date_start?: string;
  /** Optional Endzeitpunkt, bis zu dem Trades zurückgeliefert werden. */
  date_end?: string;
  /** Optional Seitenzahl zum Blättern innerhalb der Ergebnisseiten */
  page?: number;
}

export interface ExecuteTradeParams {
  /** ID des Angebots */
  order_id: string;
  /** Handelspaar (s. Tabelle Handelspaare) */
  trading_pair: string;
  /** Angebots-Typ */
  type: string;
  /** Menge der Coins */
  amount: number;
}
