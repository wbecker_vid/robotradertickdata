import { JsonProperty } from '@vid/core';

export type OHLCEntry = {
  t: number, o?: number, h?: number, l?: number, c: number, v?: number
};

export type ChartEntry = {
  date: string, time: number, close: number, asks: number, bids: number,
};

export type  ModifierType = 'map' | 'movingAverage';

export type  OHLCDataKey = 'Price' | 'Asks' | 'Bids';

export type  IndicatorKey = 'BidsClose' | 'BidsCloseMa' | 'AsksCloseMa' | 'AsksVolume' | 'BidsVolume' | 'HMAFast' | 'HMASlow'
  | 'HMACross' | 'RSI' | 'RSIHMASlow' | 'macdMacd' | 'macdSignal' | 'macdCross' | 'HMARandC';

export type  Algorythm = 'RandS' | 'hullMovingAverage' | 'weightedMovingAverage' | 'movingAverage' | 'macd' | 'macdMacd'
  | 'macdSignal' | 'chaikin' | 'rsi' | 'stochRSI' | 'divergence' | 'cross' ;

export type RandSParameters = { data?: number[], closeMA: number[], fastHMA: number[], RSIslowHMA: number[] };
export type AverageParameters = { data?: number[], size?: number };
export type MacdParameters = { data?: number[], fast_ma: number, slow_ma: number, signal: number };
export type ChaikinParameters = { data?: OHLCEntry[], size: number };
export type RsiParameters = { data?: OHLCEntry[] | number[], size: number };  // TODO OHLCEntry[] only
export type CompareParameters = { data?: number[], dataRight: number[] };

export class IndicatorParameters {

  @JsonProperty( 'data' ) data?: OHLCEntry[] | number[] | undefined = undefined;

  @JsonProperty( 'dataRight' ) dataRight?: number[] | undefined = undefined;

  @JsonProperty( 'closeMA' ) closeMA?: number[] | undefined = undefined;

  @JsonProperty( 'fastHMA' ) fastHMA?: number[] | undefined = undefined;

  @JsonProperty( 'RSIslowHMA' ) RSIslowHMA?: number[] | undefined = undefined;

  @JsonProperty( 'size' ) size?: number | undefined = undefined;

  @JsonProperty( 'fast_ma' ) fast_ma?: number | undefined = undefined;

  @JsonProperty( 'slow_ma' ) slow_ma?: number | undefined = undefined;

  @JsonProperty( 'signal' ) signal?: number | undefined = undefined;
}

export class ModifierInterface {
  @JsonProperty( 'type' ) type?: 'map' | 'indicator' | undefined = undefined;

  @JsonProperty( 'mapParameter' ) mapParameter?: string | undefined = undefined;

  @JsonProperty( 'indicator' ) indicator?: Algorythm | undefined = undefined;

  @JsonProperty( {
    name: 'indicatorParameters',
    clazz: IndicatorParameters
  } ) indicatorParameters?: IndicatorParameters | undefined = undefined;

}

export class InputInterface {
  @JsonProperty( 'parameter' ) parameter?: string | undefined = undefined;

  @JsonProperty( 'dataKey' ) dataKey?: OHLCDataKey | undefined = undefined;

  @JsonProperty( {
    name: 'modifier',
    clazz: ModifierInterface
  } ) modifier?: ModifierInterface[] | undefined = undefined;
}

export class ParameterObject {
  @JsonProperty( 'data' ) data?: InputInterface | undefined = undefined;

  @JsonProperty( 'dataRight' ) dataRight?: InputInterface | undefined = undefined;

  @JsonProperty( 'closeMA' ) closeMA?: InputInterface | undefined = undefined;

  @JsonProperty( 'fastHMA' ) fastHMA?: InputInterface | undefined = undefined;

  @JsonProperty( 'RSIslowHMA' ) RSIslowHMA?: InputInterface | undefined = undefined;
}

export class IndicatorModel {

  @JsonProperty( 'key' ) key: IndicatorKey | string = '';

  @JsonProperty( 'autoTradeIndicator' ) autoTradeIndicator: boolean = false;

  @JsonProperty( {
    name: 'sequence',
    clazz: InputInterface
  } ) sequence: InputInterface | undefined = undefined;

  @JsonProperty( 'indicator' ) indicator?: Algorythm | undefined = undefined;

  @JsonProperty( {
    name: 'inputs',
    clazz: InputInterface
  } ) inputs?: InputInterface[] | undefined = undefined;

}
