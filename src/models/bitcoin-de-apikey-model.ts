import { JsonProperty } from '@vid/core';

export class BitcoinDeApikeyModel {
  @JsonProperty( 'apikey' ) apikey: string = '';

  @JsonProperty( 'apisecret' ) apisecret: string = '';

  @JsonProperty( 'description' ) description: string = '';
}
