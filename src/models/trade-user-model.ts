import { JsonProperty } from '@vid/core';

export class TradeUserModel {
  @JsonProperty( 'username' ) username: string = '';

  @JsonProperty( 'is_kyc_full' ) is_kyc_full: boolean | undefined = undefined;

  @JsonProperty( 'bank_name' ) bank_name: string = '';

  @JsonProperty( 'bic' ) bic: string = '';

  @JsonProperty( 'rating' ) rating: number = NaN;

  @JsonProperty( 'amount_trades' ) amount_trades: number = NaN;

  @JsonProperty( 'trust_level' ) trust_level: string = '';

  @JsonProperty( 'seat_of_bank' ) seat_of_bank: string = '';
}
