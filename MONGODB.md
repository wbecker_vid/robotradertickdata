## File Export

    tar -cvzf henkelfileupload_folder.tar.gz henkelfileupload_folder

    wget http://api.visionate.com/henkelfileupload_folder.tar.gz

    tar -xvzf henkelfileupload_folder.tar.gz

# MongoDB

## run backup script

    /bin/bash "/opt/bitnami/apps/vidserverapi/htdocs/scripts/backup_mongo.sh"

## Import / Export MongoDB

ssh -i ~/.ssh/id_rsa bitnami@35.198.152.182

1. Per Docker im DSM eine bash shell öffnen

2. Changeto App Directory
``` shell
cd /app/src/apiApp
```

3. Start t6he Mongo Shell

``` shell
mongo

mongo --host localhost \
              --port 27017 \
              --username admin \
              --password EuPTAhov4cyMzbEDTWxa

mongo admin --username root -p
    root:PN2Ui6A7oRc9

## sonepar
mongo admin --username admin -p
    admin:EuPTAhov4cyMzbEDTWxa

# create db
use vidData
db.createUser(
   {
     user: "user",
     pwd: "Fax68DogEtnhzMcka2ui",
     roles: [ { role: "readWrite", db: "vidData" } ]
   }
 )

db.runCommand(
   {
     find: "pages",
     filter: { id: "13a90e2df6eb993d" }
   }
)
// store the document in a variable
doc = db.pages.find("13a90e2df6eb993d")

db.pages.update({'id':"13a90e2df6eb993d"}, {'$set':{'id':"b601bbaf22a25bd6"}});

```

``` mongo shell
show dbs
use cmsData
show collections
exit
```

4. Export / Import Data
``` shell

# dump all

 mongodump --host localhost \
              --port 27017 \
              --db cmsData \
              --username user \
              --password Fax68DogEtnhzMcka2ui \
              --collection "sennheiser/ces" \
              --gzip \
              --archive=./MongoDump/sennheiser_ces.dump.gz

              mongorestore -v --host=localhost:27017 --db=cmsData --collection "sennheiser/ces" --gzip --archive=./MongoDump/sennheiser_ces.dump.gz


 mongodump --host localhost \
              --port 27017 \
              --db cmsData \
              --username user \
              --password Fax68DogEtnhzMcka2ui \
              --gzip \
              --archive=./MongoDump/cmsData.gz

              mongorestore -v --host localhost \
                                            --port 27017 \
                                            --db cmsData \
                                            --username user \
                                            --password Fax68DogEtnhzMcka2ui \
                                            --gzip \
                                            --archive=./MongoDump/cmsData.gz

 mongodump --out ./MongoDump

# dump collection
mongodump --db cmsData --collection i18n --out ./MongoDump
mongodump --db cmsData --collection projects --out ./MongoDump
mongodump --db cmsData --collection contents --out ./MongoDump

# restore dump (Before restoring the collections from the dumped backup, drops the collections from the target database. --drop does not drop collections that are not in the backup.)
mongorestore --drop ./MongoDump/2018_11_10_13_40_32

mongorestore --host localhost --port 27017 --username root --password XXXXXXXXXX --drop ./MongoDump/2018_11_10_13_40_32

# export collection to json
mongoexport --db cmsData --collection i18n --jsonArray --pretty --out ./i18n.json
mongoexport -v --host localhost \
                --port 27017 \
                --db cmsData \
                --username user \
                --password Fax68DogEtnhzMcka2ui \
                --collection "sennheiser/ces" \
                 --jsonArray \
                 --pretty \
                 --out ./MongoDump/sennheiser_ces.json

# import collection from json
mongoimport --db cmsData --collection i18n --jsonArray --drop ./MongoExport/i18n.json
mongoimport --db cmsData --collection i18n --jsonArray --drop ./i18n.json

mongoimport -v --host localhost \
                --port 27017 \
                --db cmsData \
                --username user \
                --password Fax68DogEtnhzMcka2ui \
                --collection "sennheiser/ces" \
                 --jsonArray \
                 --drop \
                 ./MongoDump/sennheiser_ces.json

```

## Example Update Many
```
db.i18n.updateMany({ project : "5d8d60d4a52d3851" },{ $set: { project : "4ed9c51a97817874" } });
db.i18n.updateMany({ project : "f9a1dc7bbb2e78b1" },{ $set: { project : "0c52408cc7283998" } });

db.templates.deleteMany( { 'mandant': 'henkel' });

show dbs
use cmsData
show collections
db['sonepar/visitors'].deleteMany( { "t": { $lt: 1549019757000 } } );


show dbs
use vidData
show collections
db['bitcoinde/charts'].find( { "t": { $lt: 1555613539227 } } );
db['bitcoinde/charts'].deleteMany( { "t": { $lt: 1555613539227 } } );



db['bitcoinde/charts'].find( { "_id": "c4773768186feb72" } );

db['users'].find();

db.orders.deleteMany( { "stock" : "Brent Crude Futures", "limit" : { $gt : 48.88 } } );

```
