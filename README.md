npm i robotradertickdata

## query 48 hours

    https://data.visionate.com/bitcoinde/charts?{"$sort": {"t": -1},"$limit": 576}

# api.visionate.com

"robotradertickdata"

## start backend

    ssh -i ~/.ssh/id_rsa bitnami@35.198.152.182
    cd /opt/bitnami/apps/viddata/htdocs
   npm i @vid/tickdata@1.0.16


## backup Mongo

    #terminal
    bash /opt/bitnami/apps/viddata/htdocs/scripts/viddata/backup_mongo.sh


### Connect To The MongoDB Database

    mongo admin --username root -p

### Connect via gcloud

    gcloud compute --project "visionate-gmbh" ssh --zone "europe-west3-a" "vid-cms-vm"

### SSH connect

    ssh -i ~/.ssh/id_rsa bitnami@35.198.152.182
    cd /opt/bitnami/apps/vidserverapi/htdocs

### Manage Users

[manage users on linux](https://www.howtogeek.com/50787/add-a-user-to-a-group-or-second-group-on-linux/)

Bei gcloud angelegte Public keys erzeugen neue user ...

    # list all users and groups
    cut -d: -f1 /etc/passwd | xargs groups

    # add user to group (add rf to bitnami
    sudo usermod -a -G bitnami rf

    # remove user from group
    sudo gpasswd -d rf google-sudoers
    # => Removing user rf from group google-sudoers

### access mean via tunnel

    # express
    ssh -N -L 3000:127.0.0.1:3000 -i ~/.ssh/id_rsa bitnami@35.198.152.182

    # RockMongo
    ssh -N -L 8888:127.0.0.1:80 -i ~/.ssh/id_rsa bitnami@35.198.152.182

    http://127.0.0.1:8888/rockmongo

How To Start Or Stop The Services?
Each Bitnami stack includes a control script that lets you easily stop, start and restart services. The script is located at /opt/bitnami/ctlscript.sh. Call it without any service name arguments to start all services:

    sudo /opt/bitnami/ctlscript.sh start

Or use it to restart a single service, such as Apache only, by passing the service name as argument:

    sudo /opt/bitnami/ctlscript.sh restart apache

Use this script to stop all services:

    sudo /opt/bitnami/ctlscript.sh stop

Restart the services by running the script without any arguments:

    sudo /opt/bitnami/ctlscript.sh restart

Obtain a list of available services and operations by running the script without any arguments:

    sudo /opt/bitnami/ctlscript.sh

## Lösung bereitgestellt von Bitnami

    Site address	http://35.198.152.182/
    Admin user	root
    Instance	vid-cms-vm
    Instance zone	europe-west3-a
    Instance machine type	f1-micro

## Software

    Debian (9)
    Git (2.17.1)
    Apache (2.4.33)
    ImageMagick (6.9.8)
    MongoDB (3.6.5)
    Node.js (8.11.2)
    OpenSSL (1.0.2o)
    PHP (5.6.36)
    Python (2.7.15)
    RockMongo (1.1.7)
    SQLite (3.18.0)

## Erste Schritte mit M.E.A.N. Certified by Bitnami

### Open HTTP traffic
This firewall rule is not enabled. To allow specific network traffic from the Internet,
create a firewall rule to open HTTP traffic for target tag "vid-cms-tcp-80". Learn more
If you are using Google Cloud SDK, type the following command in the terminal:

    $gcloud --project=visionate-gmbh compute firewall-rules create "vid-cms-tcp-80" --network default --allow tcp:80 --target-tags "vid-cms-tcp-80"
    $gcloud --project=visionate-gmbh compute firewall-rules create "ftps-tcp-990" --network default --allow tcp:990 --target-tags "ftps-tcp-990"
    $gcloud --project=visionate-gmbh compute firewall-rules create "ftp-tcp-21" --network default --allow tcp:21 --target-tags "ftp-tcp-21"
    $gcloud --project=visionate-gmbh compute firewall-rules create default-allow-websockets --network default --allow tcp:65080 --target-tags "websocket"  --description "Allow Websocket traffic on port 65080"

### Dokumentation
Getting Started  [https://docs.bitnami.com/google/infrastructure/mean/]
Get started with Bitnami MEAN Stack.

### @eaDir Probleme beim kopieren von Files

unter MAC

    defaults write com.apple.desktopservices DSDontWriteNetworkStores true

mit dem folgenden Befehl kann nach  @eaDir-Verzeichnissen gesucht werden (reine Anzeige – es wird nichts verändert!).

    find . -type d -name "@eaDir"

Wenn die Verzeichnisse auch gleich gelöscht werden sollen, kann dieser (erweiterte) Befehl genutzt werden:

    find . -type d -name "@eaDir" -print0 | xargs -0 rm -rf


## GIT

    git config --global user.email "wolfgang.becker@visionate.com"
    git config --global user.name "Wolfgang Becker"
    git config credential.helper store
    git config credential.helper 'cache --timeout=3600'

    git init
    git add --all

    git commit -m "Initial Commit"

    git remote add origin https://wbecker_vid:bar-arr-od-t@bitbucket.org/visionate/vid-server-api.git


    git branch --set-upstream-to=origin/master master
    git pull --allow-unrelated-histories
    git push -u origin master


## UPDATE from GIT

    git pull

